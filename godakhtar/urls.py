from django.conf.urls import url, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from rest_framework import routers

from inquiry.views import get_accounts
from . import views

admin.site.site_header = 'GODAKHTAR'
admin.site.site_title = "GODAKHTAR"
admin.site.index_title = "GODAKHTAR"
router = routers.DefaultRouter()
router.register(r'', views.UserListViewSet)

urlpatterns = i18n_patterns(
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^beta-version/', admin.site.urls),
    url(r'^api/v0/customers/', include('customers.urls')),
    url(r'^api/v0/products/', include('products.urls')),
    url(r'^api/v0/inquiry/', include('inquiry.urls')),
    url(r'^api/v0/process/', include('process.urls')),
    url(r'^api/v0/attachments/', include('attachments.urls')),
    url(r'^api/v0/accounts/$', get_accounts, name='get_accounts'),
    url(r'^api/v0/rest-auth/', include('rest_auth.urls')),
    url(r'^api/v0/userlist/', include(router.urls)),
)
