from rest_framework import serializers

from customers.models import Customer
from customers.serializers import CustomerSerializer
from inquiry.models import Inquiry, InquiryAttachments, InquiryProducts, InquiryProductAttributes, InquiryOption


class InquiryOptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = InquiryOption
        fields = '__all__'


class InquiryAttachmentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = InquiryAttachments
        fields = "__all__"


class InquiryProductAttributesSerializer(serializers.ModelSerializer):
    class Meta:
        model = InquiryProductAttributes
        fields = "__all__"


class InquiryProductsSerializer(serializers.ModelSerializer):
    inquiry_product_attributes = InquiryProductAttributesSerializer(many=True, read_only=True)

    class Meta:
        model = InquiryProducts
        fields = "__all__"


class InquirySerializer(serializers.ModelSerializer):
    inquiry_products = InquiryProductsSerializer(many=True, read_only=True)
    inquiry_attachments = InquiryAttachmentsSerializer(many=True, read_only=True)
    customer = CustomerSerializer(read_only=True)
    inquiry_options = InquiryOptionSerializer(many=True, read_only=True)
    customer_id = serializers.PrimaryKeyRelatedField(
        queryset=Customer.objects.all(), source='customer', write_only=True)

    class Meta:
        model = Inquiry
        fields = '__all__'


class InquiryProductsHistorySerializer(serializers.ModelSerializer):
    inquiry_product_attributes = InquiryProductAttributesSerializer(many=True, read_only=True)
    inquiry = InquirySerializer(read_only=True)

    class Meta:
        model = InquiryProducts
        fields = "__all__"
