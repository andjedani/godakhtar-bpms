from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated


@permission_classes([IsAuthenticated])
class Process(models.Model):
    DOCUMENT_TYPE_CHOICES = (
        ('I', _('Inquiry')),
        ('O', _('Order')),
    )
    timestamp = models.DateTimeField(auto_now_add=True)
    document_type = models.CharField(max_length=1, choices=DOCUMENT_TYPE_CHOICES)
    document_id = models.IntegerField(blank=False, null=False)
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    state = models.IntegerField(default=0)
