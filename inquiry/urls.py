from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from inquiry import views as inquiry_views

router = routers.DefaultRouter()
router.register(r'^general', inquiry_views.InquiryViewSet)
router.register(r'^user', inquiry_views.UserInquiryViewSet)
router.register(r'^products', inquiry_views.InquiryProductsViewSet)
router.register(r'^product_attributes', inquiry_views.InquiryProductAttributesViewSet)
router.register(r'^attachments', inquiry_views.InquiryAttachmentsViewSet)
router.register(r'^options', inquiry_views.InquiryOptionsViwSet)
urlpatterns = [
    url(r'', include(router.urls)),
    url(r'^(?P<inquiry_id>[0-9]+)/addproduct/?$',
        inquiry_views.add_product_to_inquiry, name='add-product-to-inquiry'),
    url(r'^(?P<inquiry_id>[0-9]+)/deleteproduct/?$',
        inquiry_views.delete_product_from_inquiry, name='delete-product-from-inquiry'),
    url(r'^setstate/?$', inquiry_views.set_inquiry_state, name='set-inquiry-state'),
    url(r'^import/?$', inquiry_views.import_excel_file, name='import-excel-file'),
    url(r'^(?P<inquiry_id>[0-9]+)/export/?$', inquiry_views.export_excel_file, name='export-excel-file'),
    url(r'^(?P<inquiry_id>[0-9]+)/financial_offer_cover/?$', inquiry_views.financial_offer_cover_pdf,
        name='financial-offer-cover'),
    url(r'^(?P<inquiry_id>[0-9]+)/financial_offer_details/?$', inquiry_views.financial_offer_details_pdf,
        name='financial-offer-details'),
    url(r'^(?P<inquiry_id>[0-9]+)/technical_offer_cover/?$', inquiry_views.technical_offer_cover_pdf,
        name='technical-offer-cover'),
    url(r'^(?P<inquiry_id>[0-9]+)/technical_offer_details/?$', inquiry_views.technical_offer_details_pdf,
        name='technical-offer-details'),
]

# urlpatterns += [
#     path('api-auth/', include('rest_framework.urls')),
# ]
