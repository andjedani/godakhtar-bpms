from datetime import timedelta

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _

from attachments.models import Attachment
from customers.models import Customer, KeyPerson, Personnel
from products.models import Product, Attribute

INQUIRY_LEVEL_CHOICES = (
    ('s', _('Small')), ('m', _('Medium')), ('l', _('Large')))

CURRENCY_CHOICES = (
    ('r', _('IR Rial')), ('d', _('US Dollar')), ('e', _('Euro')), ('u', _('Yuan'))
)
INQUIRY_SUPPLY_POLICY_CHOICES = (('v', _('Inventory')), ('p', _('Production')), ('i', _('Internal Supply')),
                                 ('d', _('Product Development')), ('f', _('Foreign Supply')))

INQUIRY_TYPE_CHOICES = (('v', _('Valve')), ('p', _('Peripheral')), ('s', _('Spare Parts')),
                        ('e', _('Education')), ('r', _('Repair')))

INQUIRY_CUSTOMER_PRIORITY_CHOICES = (
    ('r', _('shipping_ready')),
    ('p', _('Production')),
    ('s', _('Supply')),
    ('f', _('Fast Delivery'))
)

INQUIRY_TECH_LEVEL_CHOICES = (
    ('l', _('Low Tech')),
    ('m', _('Mid Tech')),
    ('h', _('High Tech'))
)

BOND_TYPE_CHOICES = (
    ('c', _('Cheque')),
    ('s', _('Promissory note')),
    ('b', _('Bank Warrant')),
    ('o', _('Other'))
)

INQUIRY_OPTION_CHOICES = (
    ('n', _('Non Destructive Examinations')),
    ('d', _('Documentations')),
    ('i', _('Inspections')),
)


class Inquiry(models.Model):
    previous_version = models.ForeignKey("Inquiry", verbose_name=_('Previous Version'), on_delete=models.PROTECT,
                                         null=True, related_name='inquiry_previous_version')
    next_version = models.ForeignKey("Inquiry", verbose_name=_('Next Version'), on_delete=models.PROTECT, null=True,
                                     related_name='inquiry_next_version')

    state = models.IntegerField(default=0, verbose_name=_('Inquiry State'), blank=True, null=True)
    assignee = models.ForeignKey(get_user_model(), verbose_name=_('Assignee'), on_delete=models.PROTECT, null=True)
    score = models.PositiveSmallIntegerField(default=0, blank=True, null=True)

    # Stage 1: Customer Selection
    customer = models.ForeignKey(
        Customer, on_delete=models.CASCADE, verbose_name=_('Customer'))
    key_person = models.ForeignKey(
        KeyPerson, on_delete=models.SET_NULL, verbose_name=_('Corresponding Key Person'), blank=True, null=True)
    contact_person = models.ForeignKey(
        Personnel, on_delete=models.SET_NULL, verbose_name=_('Corresponding Expert'), blank=True, null=True)
    channel = models.CharField(max_length=80, blank=True, null=True)

    # Stage 2: Initial Review
    customer_ref_id = models.CharField(max_length=80, blank=True, null=True,
                                       verbose_name=_('Customer Reference Number'))
    purpose = models.CharField(
        max_length=1, null=True, blank=True, verbose_name=_('Inquiry Purpose'))
    level = models.CharField(max_length=1, null=True,
                             blank=True, verbose_name=_('Inquiry Level'))
    policy = models.CharField(max_length=6, null=True,
                              blank=True, verbose_name=_('Supply Policy'))
    other_policy = models.CharField(max_length=255, null=True,
                                    blank=True, verbose_name=_('Other Supply Policies'))
    inquiry_type = models.CharField(
        max_length=6, null=True, blank=True, verbose_name=_('Inquiry Type'))
    other_inquiry_type = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_('Other Inquiry Types'))

    # Stage 2: -> Project Information
    consultant_name = models.CharField(
        max_length=127, blank=True, null=True, verbose_name=_('Consultant Name'))
    client_name = models.CharField(
        max_length=127, blank=True, null=True, verbose_name=_('Client Name'))
    project_name = models.CharField(
        max_length=127, blank=True, null=True, verbose_name=_('Project Name'))

    # Stage 2: -> In case of Rejection
    rejected = models.BooleanField(default=False)
    reject_reason = models.TextField(null=True, blank=True)
    rejection_verified = models.BooleanField(default=False)
    rejection_letter_sent = models.BooleanField(default=False)
    rejection_letter = models.FileField(blank=True, null=True)

    # Stage 2: -> Sales Expert
    sales_expert = models.ForeignKey(get_user_model(), related_name="sales_expert", null=True,
                                     verbose_name=_("Sales Expert"),
                                     on_delete=models.SET_NULL)

    deadline = models.CharField(max_length=30, blank=True, null=True)

    account_number = models.CharField(max_length=127, blank=True, null=True)

    ##########################
    # Stage 3: Detailed Review

    # Stage 3: -> Bidding documents and Trade Conditions
    contract_validation = models.CharField(max_length=1, null=True, blank=True,
                                           verbose_name=_("Contract Validation Mechanism"))
    payment_type = models.CharField(
        max_length=127, blank=True, null=True, verbose_name=_('Payment Type'))
    delivery_address = models.CharField(
        max_length=255, blank=True, null=True, verbose_name=_('Delivery Address'))
    validity_duration = models.CharField(
        max_length=10, blank=True, null=True, verbose_name=_('Validity Duration'))
    delivery_duration = models.CharField(
        max_length=10, blank=True, null=True, verbose_name=_('Delivery Duration'))
    vendor_list = models.CharField(
        max_length=127, blank=True, null=True, verbose_name=_('Vendor List'))

    # Stage 3: -> Customer Priorities
    origin = models.CharField(max_length=255, blank=True, null=True, verbose_name=_('Origin'))
    customer_priority = models.CharField(max_length=255, blank=True, null=True, verbose_name=_('Customer Priorities'))
    priority_shipping_ready = models.BooleanField(default=False)
    priority_production = models.BooleanField(default=False)
    priority_supply = models.BooleanField(default=False)
    priority_fast_delivery = models.BooleanField(default=False)

    # Stage 3: Bonds
    bid_bond_value = models.BigIntegerField(null=True, blank=True, verbose_name=_("Bid Bond Value"))
    bid_bond_type = models.CharField(max_length=1, choices=BOND_TYPE_CHOICES, null=True, blank=True,
                                     verbose_name=_("Bid Bond Type"))
    payment_bond_value = models.BigIntegerField(null=True, blank=True, verbose_name=_("Payment Bond Value"))
    payment_bond_type = models.CharField(max_length=1, choices=BOND_TYPE_CHOICES, null=True, blank=True,
                                         verbose_name=_("Payment Bond Type"))
    performance_bond_value = models.BigIntegerField(null=True, blank=True, verbose_name=_("Performance Bond Value"))
    performance_bond_type = models.CharField(max_length=1, choices=BOND_TYPE_CHOICES, null=True, blank=True,
                                             verbose_name=_("Performance Bond Type"))

    # Stage 3: Terms & Conditions
    packaging = models.CharField(
        max_length=127, blank=True, null=True, verbose_name=_('Packaging'))
    guaranty = models.CharField(
        max_length=127, blank=True, null=True, verbose_name=_('Guaranty'))
    warranty = models.CharField(
        max_length=127, blank=True, null=True, verbose_name=_('Warranty'))
    qa_documents = models.CharField(
        max_length=127, blank=True, null=True, verbose_name=_('QA Documents'))
    expires_in = models.DurationField(default=timedelta(days=21))
    other_terms = models.TextField(null=True, blank=True)

    # Stage 4: Other Documents Review
    engineering_requirements = models.CharField(max_length=127, blank=True, null=True,
                                                verbose_name=_('Engineering Requirements'))
    engineering_requirements_attachment = models.FileField(null=True, blank=True)
    bidding_documents_review = models.TextField(null=True, blank=True)
    bidding_documents_review_attachment = models.FileField(null=True, blank=True)

    inquiry_description = models.TextField(blank=True, null=True)
    currency = models.CharField(max_length=1, default='r', choices=CURRENCY_CHOICES)

    # Stage 5: Maali
    financial_offer_note = models.TextField(blank=True, null=True,
                                            verbose_name=_('Financial Offer Note'))

    detailed_valves_description = models.TextField(null=True, blank=True)
    valves_testing = models.TextField(null=True, blank=True)
    valves_types = models.TextField(null=True, blank=True)
    non_destructive_examination = models.TextField(null=True, blank=True)
    optional_equipments = models.TextField(null=True, blank=True)
    documentation_valves = models.TextField(null=True, blank=True)
    painting_valve = models.TextField(null=True, blank=True)
    exceptions_comments = models.TextField(null=True, blank=True)

    def clear_products(self):
        print(str(self.inquiry_products))
        self.inquiry_products.all().delete()

    def replace_products(self, inquiry_products):
        pass

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.pk:
            self.score = 0
            super().save()
            return

        inquiry_level_score = 0
        if self.level:
            inquiry_level_scores = {'l': 10, 'm': 20, 'h': 30, '0': 10, '1': 20, '2': 30}
            inquiry_level_score = inquiry_level_scores[str(self.level)]

        customer_priority_score = 0
        if self.customer_priority:
            customer_priority_scores = {'l': 5, 'm': 10, 'h': 15}
            customer_priority_score = customer_priority_scores[str(self.customer.priority)]

        purpose_score = 0
        if self.purpose:
            purpose_scores = {'s': 20, 'b': 10, 'e': 5}
            purpose_score = purpose_scores[str(self.purpose)]

        policy_score = 0
        if self.policy:
            policy = str(self.policy)
            if "2" in policy:
                policy_score = 5
            if "4" in policy:
                policy_score = 10
            if "3" in policy:
                policy_score = 15
            if "0" in policy or "1" in policy:
                policy_score = 20

        self.score = inquiry_level_score + customer_priority_score + purpose_score + policy_score

        super().save()

    class Meta:
        ordering = ['-score', 'deadline', '-level', 'state', 'assignee', 'id']


class InquiryProducts(models.Model):
    row_number = models.IntegerField(default=-1, verbose_name=_('Row Number'))
    inquiry = models.ForeignKey(
        Inquiry, on_delete=models.CASCADE, related_name='inquiry_products')
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, related_name='product_inquiries')
    quantity = models.PositiveIntegerField(blank=False, null=False)
    unit_price = models.BigIntegerField(blank=True, null=True)
    margin = models.FloatField(null=True, blank=True)
    weight = models.BigIntegerField(null=True, blank=True)
    tech_level = models.CharField(max_length=1, default='l', null=True, blank=True)
    item_no = models.CharField(max_length=40, verbose_name=_('TAG Number'), null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    valid = models.BooleanField(default=True)

    class Meta:
        ordering = ['row_number', 'id']


class InquiryProductAttributes(models.Model):
    inquiry_product = models.ForeignKey(InquiryProducts, on_delete=models.CASCADE, null=False,
                                        related_name="inquiry_product_attributes")
    attribute = models.ForeignKey(Attribute, on_delete=models.CASCADE, null=False)
    attribute_value = models.CharField(max_length=63, null=False, blank=False, verbose_name=_('Attribute Value'))

    # def save(self, *args, **kwargs):
    # if self.attribute.type.lower() != 'c':
    #     return super().save(*args, **kwargs)

    # if self.attribute_value in self.attribute.attribute_choices.all():
    # return super().save(*args, **kwargs)

    # raise ValueError('Value ' + str(self.attribute_value) + ' is not Allowed for ' + str(self.attribute.name))

    class Meta:
        unique_together = ('inquiry_product', 'attribute')


class InquiryAttachments(models.Model):
    attachment = models.ForeignKey(Attachment, on_delete=models.CASCADE, null=False,
                                   related_name="inquiry_attachments")
    inquiry = models.ForeignKey(Inquiry, on_delete=models.CASCADE, null=False)
    field_name = models.CharField(max_length=63, null=True, blank=True)


class InquiryOption(models.Model):
    inquiry = models.ForeignKey(Inquiry, on_delete=models.CASCADE, null=False, related_name='inquiry_options')
    option_type = models.CharField(verbose_name=_('Option Type'), max_length=1, null=False, blank=False, choices=INQUIRY_OPTION_CHOICES)
    description = models.CharField(verbose_name=_('Description'), max_length=128, null=False, blank=False)
    unit_price = models.BigIntegerField(verbose_name=_('Unit Price'))
    quantity = models.PositiveIntegerField(verbose_name=_('Quantity'))
    comments = models.TextField(verbose_name=_('Comments'), blank=True, null=True)
