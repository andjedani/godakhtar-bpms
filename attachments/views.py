from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Attachment
import xlrd
from .serializers import AttachmentSerializer


@api_view(['GET', ])
@permission_classes([IsAuthenticated])
def get_path(request, file_id):
    attachment = Attachment.objects.get(pk=file_id)
    path = attachment.file.path
    book = xlrd.open_workbook(path)
    sheet_names = book.sheet_names()
    print(sheet_names)
    sheet1 = book.sheet_by_index(0)
    resp = {'sheet name': sheet1.name, 'cols': sheet1.ncols, 'rows': sheet1.nrows, 'r': sheet1.cell(28, 3).value}
    return Response(resp, status.HTTP_200_OK)


@permission_classes([IsAuthenticated])
class AttachmentView(viewsets.ModelViewSet):
    queryset = Attachment.objects.all()
    serializer_class = AttachmentSerializer
