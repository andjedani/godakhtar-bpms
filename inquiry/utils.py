import logging
from datetime import date

from inquiry.models import Inquiry, InquiryProducts, InquiryProductAttributes
from products.models import Product, Attribute, ATTRIBUTE_LIST
from tools.utils import clean_up_lower

log = logging.getLogger(__name__)
IMPORT_TITLE_MAP = {'valve type': 'type',
                    'size': 'size',
                    'class': 'class',
                    'end connection': 'connection',
                    'qty.': 'quantity',
                    'unit price': 'unit_price',
                    'margin': 'margin',
                    'weight': 'weight',
                    'tech level': 'tech_level',
                    'item': 'row_number',
                    'tag no.': 'item_no',
                    'description': 'description',
                    "operating device": "operating device",
                    "body&bonnet/cover": "body&bonnet/cover",
                    "obturator": "obturator",
                    "stem": "stem",
                    "seat": "seat",
                    "back seat": "back seat",
                    "spring": "spring",
                    "packing / o-ring": "packing / o-ring",
                    "bolt & nuts": "bolt & nuts",
                    "gaskets": "gaskets",
                    "obturator type": "obturator type",
                    "body construction": "body construction",
                    "seat type": "seat type",
                    "stem type": "stem type",
                    "nipples/pups": "nipples/pups",
                    "valve coating": "valve coating",
                    "service condition": "service condition",
                    "valve's accessory": "valve's accessory",
                    "man. std.": "man. std.",
                    "test std.": "test std.",
                    }
EXPORT_TITLE_MAP = dict((str(v).lower().strip(), k) for k, v in IMPORT_TITLE_MAP.items())
REPORT_TITLE_MAP = {'model': 'type',
                    'size': 'size',
                    'class': 'class',
                    'ends': 'connection',
                    'qty': 'quantity',
                    'unit_price': 'unit_price',
                    'item': 'row_number',
                    'tag_no': 'item_no',
                    'description': 'description',
                    "operation": "operating device",
                    "body_plus_connectors": "body&bonnet/cover",
                    "obturator": "obturator",
                    "acturator": "acturator",
                    "stem": "stem",
                    "seat": "seat",
                    "trim": "trim",
                    "ball": "ball",
                    "back seat": "back seat",
                    "spring": "spring",
                    "o_ring_or_lip_seal": "packing / o-ring",
                    "bolt_slash_nuts": "bolt & nuts",
                    "gaskets": "gaskets",
                    "obturator_type": "obturator type",
                    "body_construction": "body construction",
                    "seat_plus_insert": "seat type",
                    "stem_type": "stem type",
                    "nipples_slash_pups": "nipples/pups",
                    "valve_coating": "valve coating",
                    "service_condition": "service condition",
                    "valve_s_accessory": "valve's accessory",
                    "man_std": "man. std.",
                    "test_std": "test std.",
                    "loading_washer": "loading washer",
                    "total_price": "total price"
                    }
REPORT_TITLE_MAP_REVERSE = dict((str(v).lower().strip(), k) for k, v in REPORT_TITLE_MAP.items())

PRODUCT_FIELDS_LIST = {
    'product_type': 'type',
    'product_class': 'class',
    'product_connection': 'connection',
    'product_size': 'size',
}
INQUIRY_PRODUCT_FIELDS_LIST = [
    'item_no',
    'description',
    'quantity',
    'unit_price',
    'margin',
    'weight',
    'tech_level',
    'row_number',
]

REQUIRED_FIELDS_LIST = {
    'item_no': '',
    'quantity': 0,
    'description': '',
    'tag no.': '',
    'qty.': 0,
    'unit price': 0,
    'unit_price': 0,
    'margin': 0,
    'weight': 0,
    'tech level': '',
    'tech_level': '',
    'item': -1,
    'row_number': -1,
}
PRODUCT_TYPES = dict((str(v).lower().strip(), k) for k, v in dict(Product.PRODUCT_TYPE_CHOICES).items())
PRODUCT_CONNECTIONS = dict((str(v).lower().strip(), k) for k, v in dict(Product.PRODUCT_CONNECTION_CHOICES).items())


def get_inquiry_product_excel_row(inquiry_product):
    inquiry_product_attributes = InquiryProductAttributes.objects.filter(inquiry_product=inquiry_product)
    row = {}
    for field in INQUIRY_PRODUCT_FIELDS_LIST:
        reverse_key = EXPORT_TITLE_MAP[field]
        row[reverse_key] = ''
        try:
            row[reverse_key] = getattr(inquiry_product, field)
        except:
            pass

    for field in PRODUCT_FIELDS_LIST.keys():
        row[PRODUCT_FIELDS_LIST[field]] = getattr(inquiry_product.product, field)

    for inquiry_product_attribute in inquiry_product_attributes:
        row[clean_up_lower(str(inquiry_product_attribute.attribute.name))] = str(
            inquiry_product_attribute.attribute_value)

    return row


def get_products_flat_list(inquiry_products, what_for="excel"):
    flat_list = []
    for inquiry_product in inquiry_products:
        if what_for == "excel":
            flat_list.append(get_inquiry_product_excel_row(inquiry_product))
        else:
            flat_list.append(get_inquiry_product_report_row(inquiry_product))
    return flat_list


def map_title_to_field_name(title):
    if title in IMPORT_TITLE_MAP.keys():
        return IMPORT_TITLE_MAP[title]
    return title


def create_inquiry_product(inquiry_id, inquiry_product):
    product, created = Product.objects.get_or_create(
        product_type=PRODUCT_TYPES[clean_up_lower(inquiry_product['type'])],
        product_size=inquiry_product['size'],
        product_class=inquiry_product['class'],
        product_connection=PRODUCT_CONNECTIONS[clean_up_lower(inquiry_product['connection'])])

    inquiry = Inquiry.objects.get(pk=inquiry_id)
    inq_prod = InquiryProducts.objects.create(inquiry=inquiry, product=product,
                                              quantity=inquiry_product['quantity'],
                                              item_no=inquiry_product['item_no'],
                                              unit_price=inquiry_product['unit_price'],
                                              margin=inquiry_product['margin'],
                                              weight=inquiry_product['weight'],
                                              tech_level=inquiry_product['tech_level'],
                                              description=inquiry_product['description'])

    return inq_prod.pk


def get_or_create_attribute_by_name(attribute_name: str):
    matched_attributes = Attribute.objects.all().filter(name__iexact=clean_up_lower(attribute_name))

    if len(matched_attributes) < 1:
        attr, created = Attribute.objects.get_or_create(name=clean_up_lower(attribute_name).capitalize(), type='t')
        return attr.pk, created

    # if len(matched_attributes) > 1:
    #     matched_list = list()
    #     for attribute in matched_attributes:
    #         matched_list.append(attribute.name)
    #     return matched_list, ValueError(_('More than one attribute matched: ') + ', '.join(matched_list))

    matched_attributes[0].name = clean_up_lower(str(matched_attributes[0].name)).capitalize()
    matched_attributes[0].save()
    return matched_attributes[0].pk, False


def add_attribute(inquiry_product, attribute_name, attribute_value):
    attribute_id, created = get_or_create_attribute_by_name(attribute_name)
    attribute = Attribute.objects.get(pk=attribute_id)
    try:
        inquiry_product.product.available_attributes.add(attribute)
    except BaseException as e:
        log.warning(str(e))
    inq_prod_attr, created = InquiryProductAttributes.objects.get_or_create(inquiry_product=inquiry_product,
                                                                            attribute=attribute,
                                                                            attribute_value=attribute_value)
    return inq_prod_attr.pk


def detect_columns(title_row):
    inquiry_product = {
        'valve type': -1, 'size': -1, 'class': -1, 'end connection': -1,
        'qty.': -1,
        'unit price': -1,
        'margin': -1,
        'weight': -1,
        'tech level': -1,
        'tag no.': -1,
        'item': -1,
        'description': -1,
    }
    attributes = dict()
    unknown_columns = list()

    col_index = 0
    for title in title_row:
        title = clean_up_lower(title)
        column_detected = False

        if title in ATTRIBUTE_LIST:
            attributes[title] = col_index
            column_detected = True

        if title in inquiry_product.keys():
            inquiry_product[title] = col_index
            column_detected = True

        if not column_detected:
            log.warning('Unknown column: "' + title + '"')
            unknown_columns.append(col_index)

        col_index = col_index + 1

    return {'inquiry_product': inquiry_product, 'attributes': attributes, 'unknown_columns': unknown_columns}


def build_product_collection(header_data, excel_data):
    product_collection = list()
    for data in excel_data:
        row = {'inquiry_product': {}, 'attributes': {}}
        appendable = False
        for main_key in row.keys():
            for col in header_data[main_key].keys():
                if col in REQUIRED_FIELDS_LIST.keys():
                    row[main_key][map_title_to_field_name(col)] = REQUIRED_FIELDS_LIST[col]
                if header_data[main_key][col] > -1 and data[header_data[main_key][col]] != "None":
                    row[main_key][map_title_to_field_name(col)] = data[header_data[main_key][col]]
                    appendable = True
        if appendable:
            product_collection.append(row)
    return product_collection


def get_inquiry_product_report_row(inquiry_product):
    inquiry_product_attributes = InquiryProductAttributes.objects.filter(inquiry_product=inquiry_product)
    row = {}
    for field in REPORT_TITLE_MAP.keys():
        reverse_key = REPORT_TITLE_MAP[field]
        row[field] = ''
        try:
            row[field] = getattr(inquiry_product, reverse_key)
        except:
            pass

    for field in PRODUCT_FIELDS_LIST.keys():
        row[REPORT_TITLE_MAP_REVERSE[PRODUCT_FIELDS_LIST[field]]] = getattr(inquiry_product.product, field)

    for inquiry_product_attribute in inquiry_product_attributes:
        row[clean_up_lower(str(inquiry_product_attribute.attribute.name))] = str(
            inquiry_product_attribute.attribute_value)

    if row['qty'] != '' and row['unit_price'] != '' and row['qty'] is not None and row['unit_price'] is not None:
        row['total_price'] = str(int(row['qty']) * int(row['unit_price']))

    return row


def get_user_full_name(user):
    if user is not None:
        if user:
            return user.first_name + ' ' + user.last_name
    return ""


def get_inquiry_report(inquiry_id):
    try:
        inquiry_object = Inquiry.objects.get(pk=inquiry_id)
        inquiry_products_qs = InquiryProducts.objects.filter(inquiry=inquiry_object)
        attention = ""
        if inquiry_object.contact_person:
            attention = str(inquiry_object.contact_person.name)
        inquiry = {
            'to': str(inquiry_object.customer.name),
            'attention': attention,
            'from': get_user_full_name(inquiry_object.assignee),
            'sales_manager_area': get_user_full_name(inquiry_object.sales_expert),
            'your_ref': str(inquiry_object.customer_ref_id),
            'our_ref': str(inquiry_object.id),
            'note': str(inquiry_object.financial_offer_note),
            'terms_and_conditions': {
                'prices': '',
                'delivery': str(inquiry_object.delivery_duration),
                'incoterm': '',
                'payment': str(inquiry_object.payment_type),
                'warranty': str(inquiry_object.warranty),
                'packaging': str(inquiry_object.packaging),
                'validity': str(inquiry_object.validity_duration),
                'material_origin': str(inquiry_object.origin),
                'standard_terms_and_conditions_of_sales': '',
                'title_retention_clause': '',
            },
            'technical_offer_details': {
                'detailed_valves_description': str(inquiry_object.detailed_valves_description),
                'valve_testing': str(inquiry_object.valves_testing),
                'valve_types': str(inquiry_object.valves_types),
                'non_destructive_examination': str(inquiry_object.non_destructive_examination),
                'optional_equipments': str(inquiry_object.optional_equipments),
                'documentation': str(inquiry_object.documentation_valves),
                'painting': str(inquiry_object.painting_valve),
                'exceptions_and_comments': str(inquiry_object.exceptions_comments),
            },
            'project_name': str(inquiry_object.project_name),
            'products': [],
            'options': [],
            'date': date.today(),
            'total_item_quoted': '',
            'total_price': '',
            'non_destructive_examination': [],
            'documentations': [],
            'inspections': [],
        }

        if len(inquiry_products_qs) <= 0:
            inquiry['products'] = []
        else:
            inquiry['products'] = get_products_flat_list(inquiry_products_qs, "report")
            sum_qty = 0
            sum_total_price = 0
            for product in inquiry['products']:
                if product['qty'] != '':
                    sum_qty += int(product['qty'])
                if product['total_price'] != '':
                    sum_total_price += int(product['total_price'])

            inquiry['sum_qty'] = sum_qty
            inquiry['sum_total_price'] = sum_total_price

        return inquiry
    except BaseException as be:
        log.warning(str(be))
        return None
