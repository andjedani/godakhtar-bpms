import io
# from reportlab.platypus import SimpleDocTemplate, Paragraph
# from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
# from reportlab.lib.enums import TA_CENTER
from django.http import FileResponse
# from reportlab.pdfgen import canvas
from inquiry.models import Inquiry
# from reportlab.lib.pagesizes import letter, A4


class ReportFile(object):
    file_name: str
    document_title: str
    title: str
    sub_title: str



def financial_offer_2(inquiry):
    # Create a file-like buffer to receive PDF data.
    buffer = io.BytesIO()
    # doc = SimpleDocTemplate(buffer,
    #                         rightMargin=72,
    #                         leftMargin=72,
    #                         topMargin=72,
    #                         bottomMargin=72,
    #                         pagesize=A4)
    elements = []
    # styles = getSampleStyleSheet()
    # styles.add(ParagraphStyle(name='centered', alignment=TA_CENTER))

    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    # elements.append(Paragraph('Inquiry Products', styles['Heading1']))
    # for product in enumerate(inquiry['products']):
        # elements.append(Paragraph(str(product.item_no), styles['Normal']))

    # doc.build(elements)

    # Get the value of the BytesIO buffer and write it to the response.
    pdf = buffer.getvalue()
    # buffer.close()
    # return pdf
    # Create the PDF object, using the buffer as its "file."
    # p = canvas.Canvas(buffer)

    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    # p.drawString(100, 100, "Hello world.")

    # Close the PDF object cleanly, and we're done.
    # p.showPage()
    # p.save()

    # FileResponse sets the Content-Disposition header so that browsers
    # present the option to save the file.
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True, filename='hello.pdf')
