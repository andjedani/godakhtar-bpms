from django.db import models


class Attachment(models.Model):
    title = models.CharField(max_length=255, blank=True, null=True)
    file = models.FileField('Attachment', upload_to="docs/", null=False, blank=False)
    pk_in_table = models.IntegerField()
    model_name = models.CharField(max_length=127, blank=True, null=True)
    field_name = models.CharField(max_length=127, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    @property
    def filename(self):
        name = self.file.name.split("/")[1].replace('_', ' ').replace('-', ' ')
        return name
