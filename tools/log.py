import datetime
import json
import os


def logger(request):
    log_data = {
        'timestamp': datetime.datetime.now().timestamp(),
        'user': request.user.pk,
        'remote_address': request.META['REMOTE_ADDR'],
        'request_method': request.method,
        'request_path': request.get_full_path(),
        'request_body': request.data,
        'request_query_params': request.query_params,
    }

    if not os.path.exists('log'):
        os.makedirs('log')

    with open('log/log.json', 'a+') as f:
        json.dump(log_data, f, sort_keys=True, indent=4)
