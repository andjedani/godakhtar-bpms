# Generated by Django 2.2 on 2019-08-02 23:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0012_auto_20190629_1400'),
        ('inquiry', '0010_inquiry_contract_validation'),
    ]

    operations = [
        migrations.CreateModel(
            name='InquiryProductAttributes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('attribute', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='products.Attribute')),
                ('attribute_value', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='products.AttributeChoices')),
                ('inquiry_product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='inquiry_product_attributes', to='inquiry.InquiryProducts')),
            ],
        ),
    ]
