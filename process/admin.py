from django.contrib import admin
from . import models


@admin.register(models.Process)
class ProcessAdmin(admin.ModelAdmin):
    model = models.Process