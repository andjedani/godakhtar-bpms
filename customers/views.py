from rest_framework import viewsets
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated

from customers import models, serializers
from tools.log import logger


@permission_classes([IsAuthenticated])
class CustomerViewSet(viewsets.ModelViewSet):
    queryset = models.Customer.objects
    serializer_class = serializers.CustomerSerializer

    def initial(self, request, *args, **kwargs):
        logger(request)
        viewsets.ModelViewSet.initial(self, request, *args, **kwargs)


@permission_classes([IsAuthenticated])
class CustomerShortListSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Customer.objects.values_list("id", "name", "english_name", "customer_no",
                                                   "classification", "activity", "customer_size", "priority", )
    serializer_class = serializers.CustomerShortListSerializer

    def initial(self, request, *args, **kwargs):
        logger(request)
        viewsets.ReadOnlyModelViewSet.initial(self, request, *args, **kwargs)
