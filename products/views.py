from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

import tools
from inquiry.models import InquiryProducts
from inquiry.serializers import InquiryProductsHistorySerializer
from products import models, serializers
from tools.log import logger


@api_view(['GET', ])
@permission_classes([IsAuthenticated])
def get_similar_product_inquiries(request, product_id):
    print(product_id)
    product = models.Product.objects.get(pk=product_id)
    if product is None:
        return Response({}, status.HTTP_404_NOT_FOUND)
    inq_prods = InquiryProducts.objects.filter(product=product)
    product_data = InquiryProductsHistorySerializer(inq_prods, many=True).data

    resp = {"previous_inquiry_products": product_data, }
    return Response(resp, status.HTTP_200_OK)


@api_view(['GET', ])
@permission_classes([IsAuthenticated])
def product_types(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        serializer = tools.serializers.ChoicesSerializer(models.Product.PRODUCT_TYPE_CHOICES, many=True)
        return Response(serializer.data)


@permission_classes([IsAuthenticated])
class AttributeViewSet(viewsets.ModelViewSet):
    queryset = models.Attribute.objects
    serializer_class = serializers.AttributeSerializer

    def get_object(self):
        product_id = self.kwargs.get('product_id', None)
        product = models.Product(pk=product_id)
        return get_object_or_404(models.Attribute, product=product)


@permission_classes([IsAuthenticated])
class ProductViewSet(viewsets.ModelViewSet):
    queryset = models.Product.objects
    serializer_class = serializers.ProductSerializer

    def get_object(self):
        product_name = self.kwargs.get('product_name', None)
        if product_name:
            product_name = str(product_name).replace('-', '/').replace('_', ' ')
            return get_object_or_404(models.Product, product_name=product_name)
        return super().get_object()

    def initial(self, request, *args, **kwargs):
        logger(request)
        viewsets.ModelViewSet.initial(self, request, *args, **kwargs)
