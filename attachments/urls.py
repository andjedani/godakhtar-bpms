from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from .views import AttachmentView, get_path

router = routers.DefaultRouter()
router.register(r'', AttachmentView)
urlpatterns = [
    url(r'', include(router.urls)),
    url(r'^getpath/(?P<file_id>[0-9]+)/?$',
        get_path, name='get-path'),
]
