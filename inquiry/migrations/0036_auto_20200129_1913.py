# Generated by Django 2.2 on 2020-01-29 19:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('inquiry', '0035_auto_20200129_1724'),
    ]

    operations = [
        migrations.CreateModel(
            name='InquiryOption',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('option_type', models.CharField(choices=[('n', 'Non Destructive Examinations'), ('d', 'Documentations'), ('i', 'Inspections')], max_length=1, verbose_name='Option Type')),
                ('description', models.CharField(max_length=128, verbose_name='Description')),
                ('unit_price', models.BigIntegerField(verbose_name='Unit Price')),
                ('quantity', models.PositiveIntegerField(verbose_name='Quantity')),
                ('comments', models.TextField(blank=True, null=True, verbose_name='Comments')),
                ('inquiry', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='inquiry_options', to='inquiry.Inquiry')),
            ],
        ),
        migrations.AddField(
            model_name='inquiryproducts',
            name='valid',
            field=models.BooleanField(default=True),
        ),
        migrations.DeleteModel(
            name='Offers',
        ),
    ]
