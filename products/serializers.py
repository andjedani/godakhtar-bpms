from rest_framework import serializers

from inquiry.serializers import InquiryProductsSerializer
from products.models import Attribute, AttributeChoices, Product


class AttributeChoicesSerializer(serializers.ModelSerializer):
    class Meta:
        model = AttributeChoices
        fields = '__all__'


class AttributeSerializer(serializers.ModelSerializer):
    attribute_choices = AttributeChoicesSerializer(many=True)

    class Meta:
        model = Attribute
        fields = ('id', 'name', 'type', 'validator', 'attribute_choices')


class ProductHistorySerializer(serializers.ModelSerializer):
    product_inquiries = InquiryProductsSerializer(read_only=True, many=True)

    class Meta:
        model = Product
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    available_attributes = AttributeSerializer(read_only=True, many=True)

    class Meta:
        model = Product
        fields = '__all__'
