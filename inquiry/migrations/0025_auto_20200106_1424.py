# Generated by Django 2.2 on 2020-01-06 14:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inquiry', '0024_inquiryproducts_currency'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='inquiry',
            options={'ordering': ['score', 'deadline', 'level', 'state']},
        ),
        migrations.AlterModelOptions(
            name='inquiryproducts',
            options={'ordering': ['id']},
        ),
        migrations.AddField(
            model_name='inquiry',
            name='score',
            field=models.PositiveSmallIntegerField(blank=True, default=0, null=True),
        ),
    ]
