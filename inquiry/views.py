from datetime import datetime

import openpyxl
from django.contrib.auth import get_user_model
from django.db import transaction
from django.http import HttpResponse
from django.template.loader import get_template
from headless_pdfkit import generate_pdf
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from inquiry import serializers
from inquiry.models import InquiryAttachments, InquiryOption
from process.models import Process
from tools.log import logger
from .utils import *

log = logging.getLogger(__name__)

PDF_OPTIONS = {
    'page-size': 'A4',
    'encoding': "UTF-8",
    'margin-top': '1in',
    'margin-right': '0.5in',
    'margin-bottom': '0.5in',
    'margin-left': '0.5in',
}


@api_view(['GET', ])
@permission_classes([IsAuthenticated])
def get_accounts(request):
    accounts = ['1234', '5678', '0100067868001']
    resp = {'accounts': accounts}
    return Response(resp, status.HTTP_200_OK)


@api_view(['POST', ])
@permission_classes([IsAuthenticated])
def add_product_to_inquiry(request, inquiry_id):
    product_type = str(request.data['product_type'])
    product_size = str(request.data['product_size'])
    product_class = str(request.data['product_class'])
    product_connection = request.data['product_connection']
    quantity = int(str(request.data['quantity']))
    item_no = ""
    if 'item_no' in request.data:
        item_no = request.data['item_no']

    product, created = Product.objects.get_or_create(product_type=product_type,
                                                     product_size=product_size,
                                                     product_class=product_class,
                                                     product_connection=product_connection)

    inquiry = Inquiry.objects.get(pk=inquiry_id)
    description = ""
    try:
        if 'description' in request.data:
            description = str(request.data['description'])
    except:
        description = "OLAGH"

    inq_prod = InquiryProducts.objects.create(inquiry=inquiry, product=product, quantity=quantity,
                                              description=description, item_no=item_no)
    resp = {"created": str(created), "product_id": product.pk, "added_id": inq_prod.pk, }

    return Response(resp, status.HTTP_200_OK)


@api_view(['DELETE', ])
@permission_classes([IsAuthenticated])
def delete_product_from_inquiry(request, inquiry_id):
    try:
        product_id = int(str(request.data['product_id']))
        product = Product.objects.get(pk=product_id)
        inquiry = Inquiry.objects.get(pk=inquiry_id)
        inq_prod = InquiryProducts.objects.get(inquiry=inquiry, product=product)
        deleting_row_id = inq_prod.pk
        inq_prod.delete()
        resp = {"deleted": True, "deleted_id": deleting_row_id}
        return Response(resp, status.HTTP_200_OK)
    except Exception as exp:
        resp = {"error": str(exp)}
        return Response(resp, status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST', ])
@permission_classes([IsAuthenticated])
def set_inquiry_state(request):
    inquiry_id = request.data['inquiry_id']
    state = request.data['state']
    user_id = request.data['user_id']
    user_model = get_user_model()
    user = user_model.objects.get(pk=user_id)
    inquiry = Inquiry.objects.get(pk=inquiry_id)
    with transaction.atomic():
        Process.objects.create(document_id=inquiry_id, document_type='I', user=user, state=state)
        inquiry.assignee = user
        inquiry.state = state
        inquiry.save()
    resp = {"result": "success"}
    return Response(resp, status.HTTP_200_OK)


@permission_classes([IsAuthenticated])
class UserInquiryViewSet(viewsets.ModelViewSet):
    queryset = Inquiry.objects.all()
    serializer_class = serializers.InquirySerializer

    def get_queryset(self):
        queryset = self.queryset
        query_set = queryset.filter(assignee=self.request.user)
        return query_set

    def initial(self, request, *args, **kwargs):
        logger(request)
        viewsets.ModelViewSet.initial(self, request, *args, **kwargs)


@permission_classes([IsAuthenticated])
class InquiryViewSet(viewsets.ModelViewSet):
    queryset = Inquiry.objects.all()
    serializer_class = serializers.InquirySerializer

    def initial(self, request, *args, **kwargs):
        logger(request)
        viewsets.ModelViewSet.initial(self, request, *args, **kwargs)


@permission_classes([IsAuthenticated])
class InquiryProductsViewSet(viewsets.ModelViewSet):
    queryset = InquiryProducts.objects.all().order_by('id')
    serializer_class = serializers.InquiryProductsSerializer
    http_method_names = ['get', 'post', 'head', 'put', 'patch']

    def initial(self, request, *args, **kwargs):
        logger(request)
        viewsets.ModelViewSet.initial(self, request, *args, **kwargs)


@permission_classes([IsAuthenticated])
class InquiryOptionsViwSet(viewsets.ModelViewSet):
    queryset = InquiryOption.objects.all()
    serializer_class = serializers.InquiryOptionSerializer
    http_method_names = ['get', 'post', 'head', 'put', 'patch', 'delete', ]

    def initial(self, request, *args, **kwargs):
        logger(request)
        viewsets.ModelViewSet.initial(self, request, *args, **kwargs)


@permission_classes([IsAuthenticated])
class InquiryProductAttributesViewSet(viewsets.ModelViewSet):
    queryset = InquiryProductAttributes.objects.all()
    serializer_class = serializers.InquiryProductAttributesSerializer
    http_method_names = ['get', 'post', 'head', 'put', 'patch']

    def initial(self, request, *args, **kwargs):
        logger(request)
        viewsets.ModelViewSet.initial(self, request, *args, **kwargs)


@permission_classes([IsAuthenticated])
class InquiryAttachmentsViewSet(viewsets.ModelViewSet):
    queryset = InquiryAttachments.objects.all()
    serializer_class = serializers.InquiryAttachmentsSerializer


@api_view(['POST', ])
def import_excel_file(request):
    inquiry_id = request.data['inquiry_id']
    excel_file = request.FILES['excel_file']
    wb = openpyxl.load_workbook(excel_file)

    worksheet = wb[wb.sheetnames[0]]

    excel_data = list()
    row_number = 0
    title_row_number = None
    header = dict()

    for row in worksheet.iter_rows():
        row_data = list()
        row_data.append(str(row_number))

        for cell in row:
            row_data.append(str(cell.value))

        if clean_up_lower(row_data[1]) == "item":
            title_row_number = row_number
            header = detect_columns(row_data)

        excel_data.append(row_data)
        row_number = row_number + 1

    product_collection = build_product_collection(header, excel_data[title_row_number + 1:])
    inquiry = Inquiry.objects.get(pk=inquiry_id)
    inquiry.clear_products()
    for product_spec in product_collection:
        inquiry_product_id = create_inquiry_product(inquiry_id=inquiry_id,
                                                    inquiry_product=product_spec['inquiry_product'])
        inquiry_product = InquiryProducts.objects.get(pk=inquiry_product_id)
        for attribute_name in product_spec['attributes'].keys():
            add_attribute(inquiry_product, attribute_name, product_spec['attributes'][attribute_name])

    resp = {"inquiry_products": product_collection}
    return Response(resp, status.HTTP_200_OK)


@api_view(['GET', ])
def export_excel_file(request, inquiry_id):
    columns = EXPORT_TITLE_MAP.keys()
    try:
        inquiry = Inquiry.objects.get(pk=inquiry_id)
        products = InquiryProducts.objects.filter(inquiry=inquiry)
        if len(products) <= 0:
            return Response({'Message': 'Inquiry has no Products defined'}, status.HTTP_404_NOT_FOUND)
        rows = get_products_flat_list(products)
    except BaseException as e:
        return Response({'Error Message': str(e)}, status.HTTP_404_NOT_FOUND)

    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    )
    response['Content-Disposition'] = 'attachment; filename={inquiry_id}-{date}.xlsx'.format(
        date=datetime.now().strftime('%Y-%m-%d'), inquiry_id=inquiry_id, )
    workbook = openpyxl.Workbook()

    worksheet = workbook.active
    worksheet.title = 'Products'

    row_num = 1

    for col_num, column_title in enumerate(columns, 1):
        cell = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title

    for row in rows:
        row_num += 1
        col_num = 1
        for col in columns:
            cell = worksheet.cell(row=row_num, column=col_num)
            col_num += 1
            cell.value = ''
            if col in row.keys():
                cell.value = row[col]

    workbook.save(response)
    response.status_code = 200

    return response


@api_view(['GET', ])
def financial_offer_cover_pdf(request, inquiry_id):
    try:
        inquiry = get_inquiry_report(inquiry_id=inquiry_id)
    except BaseException as e:
        return Response({'Error Message': str(e)}, status.HTTP_404_NOT_FOUND)

    if inquiry is None:
        return Response({'Error Message': 'Error getting Inquiry'}, status.HTTP_404_NOT_FOUND)

    template = get_template('financial_offer_cover.html')
    html = template.render(inquiry)

    pdf_options = dict()
    pdf_options.update(PDF_OPTIONS)
    pdf_options.update({'orientation': 'Portrait'})

    pdf = generate_pdf(html, pdf_options)
    response = HttpResponse(pdf, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="financial_offer_cover.pdf"'
    return response


@api_view(['GET', ])
def financial_offer_details_pdf(request, inquiry_id):
    try:
        inquiry = get_inquiry_report(inquiry_id=inquiry_id)
    except BaseException as e:
        return Response({'Error Message': str(e)}, status.HTTP_404_NOT_FOUND)

    if inquiry is None:
        return Response({'Error Message': 'Error getting Inquiry'}, status.HTTP_404_NOT_FOUND)

    template = get_template('financial_offer_details.html')
    html = template.render(inquiry)

    pdf_options = dict()
    pdf_options.update(PDF_OPTIONS)
    pdf_options.update({'orientation': 'Landscape'})

    pdf = generate_pdf(html, pdf_options)
    response = HttpResponse(pdf, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="financial_offer_details.pdf"'
    return response


@api_view(['GET', ])
def technical_offer_cover_pdf(request, inquiry_id):
    try:
        inquiry = get_inquiry_report(inquiry_id=inquiry_id)
    except BaseException as e:
        return Response({'Error Message': str(e)}, status.HTTP_404_NOT_FOUND)

    if inquiry is None:
        return Response({'Error Message': 'Error getting Inquiry'}, status.HTTP_404_NOT_FOUND)

    template = get_template('technical_offer_cover.html')
    html = template.render(inquiry)

    pdf_options = dict()
    pdf_options.update(PDF_OPTIONS)
    pdf_options.update({'orientation': 'Portrait'})

    pdf = generate_pdf(html, pdf_options)

    response = HttpResponse(pdf, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="technical_offer_cover.pdf"'
    return response


@api_view(['GET', ])
def technical_offer_details_pdf(request, inquiry_id):
    try:
        inquiry = get_inquiry_report(inquiry_id=inquiry_id)
    except BaseException as e:
        return Response({'Error Message': str(e)}, status.HTTP_404_NOT_FOUND)

    if inquiry is None:
        return Response({'Error Message': 'Error getting Inquiry'}, status.HTTP_404_NOT_FOUND)

    template = get_template('technical_offer_details.html')
    html = template.render(inquiry)

    pdf_options = dict()
    pdf_options.update(PDF_OPTIONS)
    pdf_options.update({'orientation': 'Landscape'})

    pdf = generate_pdf(html, pdf_options)

    response = HttpResponse(pdf, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="technical_offer_details.pdf"'
    return response
