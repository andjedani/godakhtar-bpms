# Generated by Django 2.2 on 2019-08-02 15:00

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('inquiry', '0008_auto_20190629_1922'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bond',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bond_type', models.CharField(max_length=1)),
                ('purpose', models.CharField(max_length=1)),
                ('amount', models.BigIntegerField(blank=True, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='inquiry',
            name='customer_priority',
        ),
        migrations.RemoveField(
            model_name='inquiry',
            name='document_review',
        ),
        migrations.RemoveField(
            model_name='inquiry',
            name='other_engineering_requirements',
        ),
        migrations.RemoveField(
            model_name='inquiry',
            name='tech_level',
        ),
        migrations.RemoveField(
            model_name='inquiry',
            name='tender_guarantee',
        ),
        migrations.AddField(
            model_name='inquiry',
            name='bid_bond_attachment',
            field=models.FileField(null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='bidding_documents_review',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='bidding_documents_review_attachment',
            field=models.FileField(null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='engineering_requirements',
            field=models.CharField(blank=True, max_length=127, null=True, verbose_name='Engineering Requirements'),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='engineering_requirements_attachment',
            field=models.FileField(null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='expires_in',
            field=models.DurationField(default=datetime.timedelta(days=21)),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='inquiry_type',
            field=models.CharField(blank=True, max_length=1, null=True, verbose_name='Inquiry Type'),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='level',
            field=models.CharField(blank=True, max_length=1, null=True, verbose_name='Inquiry Level'),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='performance_bond_attachment',
            field=models.FileField(null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='policy',
            field=models.CharField(blank=True, max_length=1, null=True, verbose_name='Supply Policy'),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='prepayment_bond_attachment',
            field=models.FileField(null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='priority_fast_delivery',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='priority_production',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='priority_shipping_ready',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='priority_supply',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='project_name',
            field=models.CharField(blank=True, max_length=127, null=True, verbose_name='Project Name'),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='purpose',
            field=models.CharField(blank=True, max_length=1, null=True, verbose_name='Inquiry Purpose'),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='reject_reason',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='rejected',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='rejection_letter',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='rejection_letter_sent',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='rejection_verified',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='sales_expert',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='sales_expert', to=settings.AUTH_USER_MODEL, verbose_name='Sales Expert'),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='terms_n_conditions_descriptions',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='inquiryproducts',
            name='margin',
            field=models.FloatField(null=True),
        ),
        migrations.AddField(
            model_name='inquiryproducts',
            name='tech_level',
            field=models.CharField(default='l', max_length=1, null=True),
        ),
        migrations.AddField(
            model_name='inquiryproducts',
            name='weight',
            field=models.BigIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='inquiry',
            name='client_name',
            field=models.CharField(blank=True, max_length=127, null=True, verbose_name='Client Name'),
        ),
        migrations.AlterField(
            model_name='inquiry',
            name='delivery_time',
            field=models.CharField(blank=True, max_length=127, null=True, verbose_name='Delivery Time'),
        ),
        migrations.AlterField(
            model_name='inquiry',
            name='packaging',
            field=models.CharField(blank=True, max_length=127, null=True, verbose_name='Packaging'),
        ),
        migrations.AlterField(
            model_name='inquiry',
            name='payment_type',
            field=models.CharField(blank=True, max_length=127, null=True, verbose_name='Payment Type'),
        ),
        migrations.AlterField(
            model_name='inquiry',
            name='vendor_list',
            field=models.CharField(blank=True, max_length=127, null=True, verbose_name='Vendor List'),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='bid_bond',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='bid_bond', to='inquiry.Bond', verbose_name='Bid Bond'),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='performance_bond',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='performance_bond', to='inquiry.Bond', verbose_name='Performance Bond'),
        ),
        migrations.AddField(
            model_name='inquiry',
            name='prepayment_bond',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='prepayment_bond', to='inquiry.Bond', verbose_name='Prepayment Bond'),
        ),
    ]
